import datetime
import os
import sys
import time

import gym
import numpy as np
import torch

from .abstract_game import AbstractGame

EMPTY = -1
WHITE = 0
BLACK = 1
SIZE = 8


class MuZeroConfig:
    def __init__(self):
        # More information is available here: https://github.com/werner-duvaud/muzero-general/wiki/Hyperparameter-Optimization

        self.seed = 0  # Seed for numpy, torch and the game



        ### Game
        self.observation_shape = (3, SIZE, SIZE)  # Dimensions of the game observation, must be 3D (channel, height, width). For a 1D array, please reshape it to (1, 1, length of array)
        self.action_space = [i for i in range(SIZE * SIZE)]  # Fixed list of all possible actions. You should only edit the length
        self.players = [i for i in range(2)]  # List of players. You should only edit the length
        self.stacked_observations = 0  # Number of previous observations and previous actions to add to the current observation

        # Evaluate
        self.muzero_player = 0  # Turn Muzero begins to play (0: MuZero plays first, 1: MuZero plays second)
        self.opponent = "random"  # Hard coded agent that MuZero faces to assess his progress in multiplayer games. It doesn't influence training. None, "random" or "expert" if implemented in the Game class



        ### Self-Play
        self.num_actors = 9  # Number of simultaneous threads self-playing to feed the replay buffer
        self.max_moves = 50  # Maximum number of moves if game is not finished before
        self.num_simulations = 50  # Number of future moves self-simulated
        self.discount = .997  # Chronological discount of the reward
        self.temperature_threshold = 15  # Number of moves before dropping the temperature given by visit_softmax_temperature_fn to 0 (ie selecting the best action). If None, visit_softmax_temperature_fn is used every time

        # Root prior exploration noise
        self.root_dirichlet_alpha = 0.25
        self.root_exploration_fraction = 0.25

        # UCB formula
        self.pb_c_base = 19652
        self.pb_c_init = 1.25



        ### Network
        self.network = "resnet"  # "resnet" / "fullyconnected"
        self.support_size = 10  # Value and reward are scaled (with almost sqrt) and encoded on a vector with a range of -support_size to support_size
        
        # Residual Network
        self.downsample = False  # Downsample observations before representation network (See paper appendix Network Architecture)
        self.blocks = 6  # Number of blocks in the ResNet
        self.channels = 64  # Number of channels in the ResNet
        self.reduced_channels_reward = 2  # Number of channels in reward head
        self.reduced_channels_value = 2  # Number of channels in value head
        self.reduced_channels_policy = 4  # Number of channels in policy head
        self.resnet_fc_reward_layers = [32]  # Define the hidden layers in the reward head of the dynamic network
        self.resnet_fc_value_layers = [32]  # Define the hidden layers in the value head of the prediction network
        self.resnet_fc_policy_layers = [32]  # Define the hidden layers in the policy head of the prediction network
        
        # Fully Connected Network
        self.encoding_size = 32
        self.fc_representation_layers = []  # Define the hidden layers in the representation network
        self.fc_dynamics_layers = [64]  # Define the hidden layers in the dynamics network
        self.fc_reward_layers = [64]  # Define the hidden layers in the reward network
        self.fc_value_layers = []  # Define the hidden layers in the value network
        self.fc_policy_layers = []  # Define the hidden layers in the policy network



        ### Training
        self.results_path = os.path.join(os.path.dirname(__file__), "../results", os.path.basename(__file__)[:-3], datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S"))  # Path to store the model weights and TensorBoard logs
        self.training_steps = 50  # Total number of training steps (ie weights update according to a batch)
        self.batch_size = 16  # Number of parts of games to train on at each training step
        self.checkpoint_interval = 10  # Number of training steps before using the model for self-playing
        self.value_loss_weight = 0.25  # Scale the value loss to avoid overfitting of the value function, paper recommends 0.25 (See paper appendix Reanalyze)
        self.training_device = "cuda" if torch.cuda.is_available() else "cpu"  # Train on GPU if available

        self.optimizer = "SGD"  # "Adam" or "SGD". Paper uses SGD
        self.weight_decay = 1e-4  # L2 weights regularization
        self.momentum = 0.9  # Used only if optimizer is SGD

        # Exponential learning rate schedule
        self.lr_init = 0.02  # Initial learning rate
        self.lr_decay_rate = 1  # Set it to 1 to use a constant learning rate
        self.lr_decay_steps = 10000



        ### Replay Buffer
        self.window_size = 10000  # Number of self-play games to keep in the replay buffer
        self.num_unroll_steps = 42  # Number of game moves to keep for every batch element
        self.td_steps = 42  # Number of steps in the future to take into account for calculating the target value
        self.use_last_model_value = True  # Use the last model to provide a fresher, stable n-step value (See paper appendix Reanalyze)

        # Prioritized Replay (See paper appendix Training)
        self.PER = True  # Select in priority the elements in the replay buffer which are unexpected for the network
        self.use_max_priority = False  # If False, use the n-step TD error as initial priority. Better for large replay buffer
        self.PER_alpha = 0.5  # How much prioritization is used, 0 corresponding to the uniform case, paper suggests 1
        self.PER_beta = 1.0



        ### Adjust the self play / training ratio to avoid over/underfitting
        self.self_play_delay = 0  # Number of seconds to wait after each played game
        self.training_delay = 0  # Number of seconds to wait after each training step
        self.ratio = None  # Desired self played games per training step ratio. Equivalent to a synchronous version, training can take much longer. Set it to None to disable it


    def visit_softmax_temperature_fn(self, trained_steps):
        """
        Parameter to alter the visit count distribution to ensure that the action selection becomes greedier as training progresses.
        The smaller it is, the more likely the best action (ie with the highest visit count) is chosen.

        Returns:
            Positive float.
        """
        return 1


class Game(AbstractGame):
    """
    Game wrapper.
    """

    def __init__(self, seed=None):
        self.env = Othello()

    def step(self, action):
        """
        Apply action to the game.
        
        Args:
            action : action of the action_space to take.

        Returns:
            The new observation, the reward and a boolean if the game has ended.
        """
        observation, reward, done = self.env.step(action)
        return observation, reward * 10, done

    def to_play(self):
        """
        Return the current player.

        Returns:
            The current player, it should be an element of the players list in the config. 
        """
        return self.env.to_play()

    def legal_actions(self):
        """
        Should return the legal actions at each turn, if it is not available, it can return
        the whole action space. At each turn, the game have to be able to handle one of returned actions.
        
        For complex game where calculating legal moves is too long, the idea is to define the legal actions
        equal to the action space but to return a negative reward if the action is illegal.        

        Returns:
            An array of integers, subset of the action space.
        """
        return self.env.legal_actions()

    def reset(self):
        """
        Reset the game for a new game.
        
        Returns:
            Initial observation of the game.
        """
        return self.env.reset()

    def render(self):
        """
        Display the game observation.
        """
        self.env.render()
        input("Press enter to take a step ")

    # def encode_board(self):
    #     return self.env.encode_board()

    def human_to_action(self):
        """
        For multiplayer games, ask the user for a legal action
        and return the corresponding action number.

        Returns:
            An integer from the action space.
        """
        col = int(input("Enter the row to play for the player {}: ".format(self.to_play())))
        row = int(input("Enter the column to play for the player {}: ".format(self.to_play())))
        choice = row * 8 + col
        print(choice)
        while choice not in self.legal_actions():
            row = int(input("Enter another column: "))
            col = int(input("Enter another row: "))
            choice = row * 8 + col
            print(choice)
        return choice

    def expert_agent(self):
        """
        Hard coded agent that MuZero faces to assess his progress in multiplayer games.
        It doesn't influence training

        Returns:
            Action as an integer to take in the current game state
        """
        return self.env.expert_action()

    def action_to_string(self, action_number):
        """
        Convert an action number to a string representing the action.

        Args:
            action_number: an integer from the action space.

        Returns:
            String representing the action.
        """
        return "Play column {}".format(action_number + 1)


def board(): return ((i, j) for j in range(SIZE) for i in range(SIZE))


class IterativeAlphaBetaPlayer(object):
    '''Alpha-beta with iterative deepening game tree search.'''

    def __init__(self, dt, evaluate):
        self.__dt = dt
        self.__evaluate = evaluate

    def play(self, game):
        start_time = time.time()
        moves = game.moves()
        if not moves:
            return moves
        if len(moves) == 1:
            return moves[0]
        for ply in range(100):
            scored_moves = [(-alphabeta_eval(game.copy().move(i, j),
                                             ply, -sys.maxsize, sys.maxsize,
                                             self.__evaluate), (i, j))
                            for i, j in moves]
            scored_moves.sort(reverse=True)
            moves = (move for score, move in scored_moves)
            if time.time() - start_time > self.__dt:
                break
        return next(moves)             # return best move


def alphabeta_eval(game, ply, alpha, beta, evaluate):
    if ply == 0 or game.over():
        return evaluate(game)
    best = -sys.maxsize
    for i, j in game.moves():
        value = -alphabeta_eval(game.copy().move(i, j),
                                ply - 1, -beta, -alpha, evaluate)
        if value > best:
            best = value
        if best > alpha:
            alpha = best
        if alpha >= beta:
            return alpha
    return best

def edge_eval(game):
    '''Evaluation function which gives more preference to edges and corners.'''

    # If the game is over give a big bonus to the winning player.
    if game.over():
        return 1000*simple_eval(game)

    # Score normal pieces 1, edges 6, and corners 11.
    score = 0
    for i, j in board():
        delta = 1
        if i == 0 or i == SIZE - 1:
            delta += 5
        if j == 0 or j == SIZE - 1:
            delta += 5
        if game.get(i, j) == game.mover():
            score += delta
        elif game.get(i, j) == game.opponent():
            score -= delta
    return score

def simple_eval(game):
    ''' Evaluation function which gives the same preference to all squares.'''
    score = 0
    for i, j in board():
        if game.get(i, j) == game.mover():
            score += 1
        elif game.get(i, j) == game.opponent():
            score -= 1
    return score


class Othello(object):
    __DIRECTIONS = [(-1, -1), (-1,  0), (-1,  1),
                    (0, -1),           (0,  1),
                    (1, -1), (1,  0), (1,  1)]

    def __init__(self):
        self.__square = np.full((SIZE, SIZE), EMPTY)
        self.__square[3][3] = self.__square[4][4] = WHITE
        self.__square[3][4] = self.__square[4][3] = BLACK
        self.__mover, self.__opponent = WHITE, BLACK

    def __str__(self):
        r = ''
        for j in range(SIZE - 1, -1, -1):
            r += '\t%d|' % j
            for i in range(SIZE):
                if self.__square[i][j] == WHITE:
                    r += ' W '
                elif self.__square[i][j] == BLACK:
                    r += ' B '
                else:
                    r += ' . '
            r += '\n'
        r += '\t +' + 24*'-' + '\n\t   0  1  2  3  4  5  6  7\n'
        return r

    def copy(self):
        c = Othello()
        c.__square = [[self.__square[i][j] for j in range(SIZE)]
                      for i in range(SIZE)]
        c.__mover, c.__opponent = self.__mover, self.__opponent
        return c

    def get(self, i, j): return self.__square[i][j]

    def switch(self):
        self.__mover, self.__opponent = self.__opponent, self.__mover

    def __flip_list(self, i, j):
        '''Get a list of the pieces flipped if a piece is set at (i, j).'''
        flip = []
        for du, dv in Othello.__DIRECTIONS:
            t = []
            u, v = i + du, j + dv
            while 0 <= u < SIZE and 0 <= v < SIZE:
                if self.__square[u][v] != self.__opponent:
                    break
                t.append((u, v))
                u, v = u + du, v + dv
            else:
                continue              # fell off board
            if self.__square[u][v] == self.__mover:
                flip += t
        return flip

    def __valid_move(self, i, j):
        '''Check if (i, j) is a valid move.'''
        if self.__square[i][j] != EMPTY:
            return False
        for du, dv in Othello.__DIRECTIONS:
            count = 0
            u, v = i + du, j + dv
            while 0 <= u < SIZE and 0 <= v < SIZE:
                if self.__square[u][v] != self.__opponent:
                    break
                count += 1
                u, v = u + du, v + dv
            else:
                continue              # fell off board
            if self.__square[u][v] == self.__mover and count > 0:
                return True
        return False

    def mover(self): return self.__mover

    def opponent(self): return self.__opponent

    def move(self, i, j):
        '''Place a piece at (i, j).'''
        self.__square[i][j] = self.__mover
        for u, v in self.__flip_list(i, j):
            self.__square[u][v] = self.__mover
        self.switch()
        return self

    def moves(self):
        '''Return a list of valid moves.'''
        return [(i, j) for i, j in board() if self.__valid_move(i, j)]

    def over(self):
        '''Check to see if the game is over.'''
        if self.moves():
            return False
        self.switch()
        moves = self.moves()
        self.switch()
        return not moves

    def getBoard(self):
        return self.__square

    def score(self):
        '''Calculate the current score.'''
        return (sum(max(self.__square[i][j], 0) for i, j in board()),
                sum(max(-self.__square[i][j], 0) for i, j in board()))

    def to_play(self):
        return self.mover()

    def reset(self):
        self.__init__()
        return self.get_observation()

    def step(self, action):
        row = action // SIZE
        col = action % SIZE
        self.move(row, col)

        done = self.have_winner() or len(self.legal_actions()) == 0

        reward = 1 if self.have_winner() else 0

        return self.get_observation(), reward, done

    def get_observation(self):
        board_player1 = np.where(self.__square == WHITE, 1.0, 0.0)
        board_player2 = np.where(self.__square == BLACK, 1.0, 0.0)
        board_to_play = np.full((8, 8), self.mover()).astype(float)
        # print(board_to_play)
        # Print(Np.array([board_player1, board_player2, board_to_play]))
        return np.array([board_player1, board_player2, board_to_play])

    def legal_actions(self):
        legal_pre = self.moves()
        legal = list(map(lambda x:x[0] * 8 + x[1], legal_pre))
        return legal

    def have_winner(self):
        return self.over()

    def expert_action(self):
        abp = IterativeAlphaBetaPlayer(3, edge_eval)
        return abp.play(self)

    def render(self):
        print(self)    
