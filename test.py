import ray
# import torch  # if torch is imported crash, otherwise run fine


@ray.remote
def f(shard):
    print(f"{shard}")


ray.init(include_webui=False)

# Work in 3.8 and 3.7
ray.get(
    [
        f.remote(shard)
        for shard in ray.util.iter.from_range(10, num_shards=4, repeat=True).shards()
    ]
)

# Crash in 3.8 but not 3.7
it = ray.util.iter.from_range(10, num_shards=4, repeat=True).shards()
ray.get([f.remote(shard) for shard in it])
